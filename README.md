# Restaurant Suggestion Wheel
Very basic restaurant suggestion wheel site made with vanilla JavaScript.

Includes [jQuery](https://jquery.com/), [Bootstrap](https://getbootstrap.com/), [Google API](https://developers.google.com/maps), [Winwheel.js](http://dougtesting.net/home), and [SweetAlert.js](https://sweetalert.js.org/).

[https://tiffeichelberger.gitlab.io/restaurant-suggestion-wheel/](https://tiffeichelberger.gitlab.io/restaurant-suggestion-wheel/) to view a demo.