// ----------------------------
// Google API
// ----------------------------
var script = document.createElement('script');
script.src = `https://maps.googleapis.com/maps/api/js?key=${KEY}&libraries=places`;
script.async = true;
document.head.appendChild(script);

var geocoder = null, service = null;
script.onload = () => {
  geocoder = new google.maps.Geocoder();
  service = new google.maps.places.PlacesService(document.createElement('div'));
}

// ----------------------------
// Fast Food
// ----------------------------
function excludeFastfood() {
  if (noFastfood == false) {
    document.getElementById("fastFoodButton").style.backgroundColor = "#545b62";
    noFastfood = true;
    return;
  }
  if (noFastfood == true) {
    document.getElementById("fastFoodButton").style.backgroundColor = "#6c757d";
    noFastfood = false;
    return;
  }
}

// ----------------------------
// Geolocation
// ----------------------------
function getLocationButton() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showLocation)
  } else { 
    swal({
      icon: "error",
      title: "Sorry!",
      text: "Geolocation is not supported by this browser."
    })
  }
}

function showLocation(position) {
  var latlng = {lat: position.coords.latitude, lng: position.coords.longitude};
  geocoder.geocode({'location': latlng}, function(results, status) {
    if (status === 'OK') {
      if (results[0]) {
        document.getElementById("location").value = results[1].formatted_address;
        enableSpin();
      } else {
        swal({
          icon: "error",
          title: "Sorry!",
          text: "I can't find your location! Try again!"
        })
      }
    } else {
      swal({
        icon: "error",
          title: "Whoops!",
          text: "Something went wrong..."
      })
    }
  });
}

var latLng = [];
var restaurants = [];
var segmentColors = [];
var keyword = "";
var noFastfood = false;
var fastFoodList = ['McDonald\'s', 'Whataburger', 'Jack\'s',
                    'Burger King', 'Hardee\'s', 'Steak \'n Shake',
                    'Wendy\'s', 'Chick-fil-A', 'Dairy Queen',
                    'Milo\'s Hamburgers', 'Five Guys', 'Rally\'s',
                    'Taco Bell', 'Sonic Drive-In', 'Krystal',
                    'Panda Express', 'Zaxby\'s'];

function getLocation() {
  var address = document.getElementById("location").value;
  return new Promise((resolve, reject) => {
    latLng = [];
    geocoder.geocode({"address": address}, function(results, status) {
      if (status == 'OK') {
        latLng.push(results[0].geometry.location.lat());
        latLng.push(results[0].geometry.location.lng());
        resolve (latLng);
      } else if (status == 'ZERO_RESULTS') {
        swal({
          icon: "error",
          title: "Sorry!",
          text: "I can't find that address! Try again!"
        })
      } else {
        swal({
          icon: "error",
          title: "Whoops!",
          text: "Something went wrong..."
        })
      }
    })
  })
}

function getKeyword(value) {
  keyword = value;
}

function getList() {
  if (document.getElementById("distance").value != "") {
    distance = (document.getElementById("distance").value) * 1609.34;
  } else {
    distance = 2 * 1609.34;
  }
  function Restaurant(fillStyle, text, googleObject) {
    this.fillStyle = fillStyle;
    this.text = text;
    this.googleObject = googleObject;
  }

  var searchRequest = {
    location: new google.maps.LatLng(latLng[0], latLng[1]),
    radius: distance,
    type: "restaurant",
    keyword: keyword,
    openNow: true
  };

  return new Promise((resolve, reject) => {
    restaurants = [];
    service.nearbySearch(searchRequest, function(results, status) {
      if (status == 'OK') {
        var colorPosition = 0;
        segmentColors = ["#e7706f", "#7de6ef", "#89f26e", "#ffff99"];
        for (length = 0; length < results.length; length++) {
          var remove = false;

          if (noFastfood == true) {
            for (fastfood = 0; fastfood < fastFoodList.length; fastfood++) {
              if (results[length].name.includes(fastFoodList[fastfood]) && !remove) {
                remove = true;
              } else {
              }
            }
          }

          if (remove) {
            delete results[length]
          } else {
            googleObject = results[length];
            if (results[length].name.length >= 22) {
              name = results[length].name.slice(0, 22).concat('...');
            } else {
              name = results[length].name;
            }

            var restaurant = new Restaurant("", name, googleObject)
            if (segmentColors.length % 2 == 0) { 
              if (colorPosition < 3) {
                restaurant.fillStyle = segmentColors[colorPosition]
                colorPosition++;
              } else {
                restaurant.fillStyle = segmentColors[colorPosition]
                colorPosition = 0;
              }
            } else {
              if (colorPosition < 2) {
                restaurant.fillStyle = segmentColors[colorPosition]
                colorPosition++;
              } else {
                restaurant.fillStyle = segmentColors[colorPosition]
                colorPosition = 0;
              }
            }
            restaurants.push(restaurant)
          }
        }
        if (restaurants.length > 0) {
          resolve (restaurants);
        } else {
          swal({
            icon: "error",
            title: "Sorry!",
            text: "There are no results matching those criteria! Try again!"
          })
        }
      } else if (status == 'ZERO_RESULTS') {
        swal({
          icon: "error",
          title: "Sorry!",
          text: "There are no results matching those criteria! Try again!"
        })
      } else {
        swal({
          icon: "error",
          title: "Whoops!",
          text: "Something went wrong..."
        })
      }
    })
  })
}

// ----------------------------
// Wheel
// ----------------------------
var wheelSpinning = false;
var theWheel;

function paintWheel() {
  return new Promise((resolve, reject) => {
    // Create new wheel object specifying the parameters at creation time.
    theWheel = new Winwheel({
      'numSegments'  : restaurants.length,    // Specify number of segments.
      'outerRadius'  : 250,                   // Set outer radius so wheel fits inside the background.
      'textFontSize' : 15,                    // Set font size as desired.
      'segments'     : restaurants,           // Define segments including colour and text.,
      'animation' :                           // Specify the animation to use.
      {
        'type'     : 'spinToStop',
        'duration' : 3,                       // Duration in seconds.
        'callbackFinished' : 'alertPrize()'
      }
    })
    resolve(theWheel);
  })
}

function populateWheel(event) {
  event.preventDefault();
  getLocation()
    .then(function() {
      getList()
        .then(function() {
          paintWheel()
            .then(function() {
              document.getElementById("canvas").scrollIntoView();
              spin()
            })
        })
    })
}

// ----------------------------
// Wheel Actions
// ----------------------------
function enableSpin() {
  if ((document.getElementById('location').value.replace(/\s/g,"") == "")) {
    document.getElementById("spin").disabled = true;
  } else {
    document.getElementById("spin").disabled = false;
  }
}

function spin() {
  theWheel.animation.spins = 2;
  theWheel.startAnimation();
}

function alertPrize() {
  var indicatedSegment = theWheel.getIndicatedSegment();
  var googleLink = "https://www.google.com/maps/dir/" + latLng[0] + ",+" + latLng[1] + "/" + (indicatedSegment.googleObject.vicinity).replace(/ /g, "+");
  swal({
    icon: "success",
    title: indicatedSegment.googleObject.name,
    buttons: {
      cancel: "OK",
      maps: {
        text: "Google Maps Link",
        value: "maps"
      }
    }
  }).then((value) => {
    if (value == "maps") {
      window.open(googleLink);
    }
  })
}